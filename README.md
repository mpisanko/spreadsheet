# Programming Task: Spreadsheet

Task description
================

Write a program to which parses a spreadsheet-like CSV file and evaluates each cell by these rules:

1.  Each cell is an expression in postfix notation (see [*Wikipedia*](https://en.wikipedia.org/wiki/Reverse_Polish_notation)).

2.  Each token in the expression will be separated by one or more spaces.

3.  One cell can refer to another cell with the {LETTER}{NUMBER} notation (e.g. “A2”, “B4”– letters refer to columns, numbers to rows).

4.  Expressions may include the basic arithmetic operators `+, -, *, /`

Your program should output another CSV file of the same dimensions containing the results of evaluating each cell to its final value. If any cell is an invalid expression, then **for that cell only** print \#ERR.

For example, for the following CSV input:
```
b1 b2 + ,2 b2 3  * -,3 ,+
a1      ,5          ,  ,7 2 /
c2 3  * ,1 2        ,  ,5 1 2 + 4  * + 3 -
```

...output something like:

```
-8,-13 ,3,#ERR
-8,5   ,0,3.5
 0,#ERR,0,14
```

There are other error conditions that your implementation should detect and handle in a manner you think appropriate.

Solution guide
==============

-   well-written and structured code

-   use only what is available in the standard and most commonly used extension libraries for your platform

-   where some detail of the task is unspecified, use your best judgement

-   assumptions or limitations in implementation are fine if documented

Deliverables
============

-   source code and test input data

-   a **short** report (½ page max) outlining approach, any highlights or limitations of the implementation, and any trade-offs or design decisions worth noting


Assumptions
===========

- CSV file must fit into memory due to the possible references in values
 
- empty lines are skipped (multiple consecutive newlines are treated as one)

- empty cell is treated as zero

- division by zero results in error rather than Infinity

- if a cycle is detected - the cells involved are resolved as error
 

Solution
========

- I followed a functional, data first approach.
 
- The universal data structure - map - was suitable and convenient due to requirement of (de)referencing other cells.

- Modelling the domain based on data - results in functions that manipulate domain data and are composed in order to transform data from input format to desired output.

- The code does not mutate any state and keeps only necessary state (the input and result data + calculation steps) to reduce incidental complexity (see [Out of the Tar Pit](http://shaffner.us/cs/papers/tarpit.pdf))
 
- The solution uses a recursive function which follows algorithm outlined in [Wikipedia](https://en.wikipedia.org/wiki/Reverse_Polish_notation) with modifications allowing referencing other expressions (or cells in a spreadsheet).

- Immutability and functional approach go hand in hand with [property testing](https://en.wikipedia.org/wiki/Property_testing) - and that's what I use for tests (along with some example based tests). I think this approach is much better than hand crafting examples - hence there is no other test input (I just provided the sample from the assignment description).

- Values are hardcoded into code - rather than provided via configuration (or component model) which would be more suitable for a realistic scenario.
 
 
Usage
=====
1. Install JDK 8
2. Install leiningen: `curl -o ~/bin/lein https://raw.githubusercontent.com/technomancy/leiningen/stable/bin/lein && chmod 755 ~/bin/lein`
3. Add ~/bin/lein to PATH `export PATH=$PATH:~/bin`
4. cd path/to/project
5. Run all tests: `lein test`
6. Run the programme using leiningen: `lein run <input.csv> <output.csv>`
7. Create java executable: `lein uberjar` (creates executable jar in target directory)
8. Run the executable jar: `java -jar target/spreadsheet-0.1.0-standalone.jar <input.csv. <output.csv>`