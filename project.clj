(defproject spreadsheet "0.1.0"
  :description "spreadsheet assignment"
  :url "https://bitbucket.org/mpisanko/spreadsheet"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.7.0"]
                 [org.clojure/test.check "0.9.0"]
                 [org.clojure/tools.reader "1.0.0-alpha2"]]
  :aot [spreadsheet.core]
  :main spreadsheet.core)
