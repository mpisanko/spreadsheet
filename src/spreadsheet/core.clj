(ns spreadsheet.core
  (:require [spreadsheet.csv.parse :refer [create-value-map create-csv]]
            [spreadsheet.expression.parse :refer [parse-expressions]])
  (:gen-class))

(defn -main
  [& args]
  (if (.exists (java.io.File. (first args)))
    (let [res (parse-expressions (create-value-map (slurp (first args))))]
      (spit (second args) (create-csv res)))
    (do (println (str "The input and output files must be valid paths."))
        (System/exit 1))))

