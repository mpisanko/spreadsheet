(ns spreadsheet.csv.converter
  (:require [clojure.string :as str]))

(def bc "base character" \a)
(def fc "integer value of base character" (int bc))
(def ds "number of digits" 10)
(def radix "the radix" 26)

(defn- next-value-character
  [v]
  (if (Character/isDigit v)
    (+ fc (Character/digit v ds))
    (+ (int v) ds)))

(defn- next-character-value
  [c]
  (- (int c) fc))

(defn- first-value-character
  [v]
  (next-value-character v))

(defn- first-character-value
  [c]
  (if (= c bc)
    (inc (next-character-value c))
    (next-character-value c)))

(defn string-to-number
  "Convert spreadsheet representation into decimal number"
  [s]
  (if-let [chars (seq (str/lower-case s))]
    (let [coll (cons (first-character-value (first chars))
                     (map next-character-value (next chars)))]
      (Integer/parseInt (str/join "" (map #(Integer/toString % radix) coll)) radix))))

(defn number-to-string
  [n]
  (when ((complement neg?) n)
    (let [s (Integer/toString n radix)
          chars (seq s)
          coll (cons (first-value-character (first chars))
                     (map next-value-character (next chars)))]
      (str/join "" (map #(char %) coll)))))