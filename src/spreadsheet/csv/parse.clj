(ns spreadsheet.csv.parse
  (:require [clojure.string :as str]
            [spreadsheet.csv.converter :refer [string-to-number number-to-string]]))

(def value-separator #",")

(def line-separator #"\n")

(def token-separator #"\s+")

(defn- split-by
  "splits string by a given separator"
  ([in sep]
   (str/split in sep))
  ([in sep opts]
   (str/split in sep opts)))

(defn parse-line
  "Parse a single line of input returning a vector of values"
  [in]
  (split-by in value-separator -1))

(defn parse-lines
  "Parse multiple lines of CSV returning vector of lines"
  [in]
   (split-by in line-separator))

(defn- parse-value
  "Parse a value cell into tokens"
  [[cell value]]
  {cell (split-by value token-separator)})

(defn- number-lines
  "Give numbers to elements"
  [elements element]
  (assoc elements (inc (count elements)) element))

(defn- name-values
  "Give each value a name and number, returns a map of cells"
  [acc [number line]]
  (merge acc (reduce #(assoc %1 (str (number-to-string (count %1)) number) %2) {} (parse-line line))))

(defn create-value-map
  "Creates a spreadsheet-like map of values mapped to their positions from CSV, eg:
    - B1 represents second value in first row
    - D2 represents fourth value in second row"
  [in]
  (let [lines (reduce number-lines {} (filter not-empty (parse-lines in)))
        value-map (reduce name-values {} lines)]
    (reduce merge {} (map parse-value value-map))))

(defn- by-key-numeric-part
  [[key _]]
  (second (str/split key #"[a-zA-Z]+")))

(defn create-csv
  "Creates CSV from a spreadsheet-like map of values mapped to their positions"
  [value-map]
  (let [sorted (group-by by-key-numeric-part (into (sorted-map) value-map))
        by-line (map val sorted)
        lines (map (partial map second) by-line)
        csv-lines (map (partial str/join value-separator) lines)
        csv (str/join (System/lineSeparator) csv-lines)]
    csv))