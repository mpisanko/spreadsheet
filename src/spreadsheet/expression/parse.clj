(ns spreadsheet.expression.parse
  (:require [clojure.string :as str]
            [clojure.tools.reader.edn :as edn]))

(def error "#ERR")
(def error-pattern (re-pattern error))
(def operators #"^[+\-*/]$")
(def link  #"^[a-z]+\d+$")
(def numeric-pattern #"^-?(\d*[\.\/e]?\d+(e|E-?\d+)?)|(Infinity)$")
(declare evaluate-expression)
(declare parse-expression)

(defn- error?
  [e]
  (re-find error-pattern (str e)))

(defn- link?
  [e]
  (re-find link (str e)))

(defn- operator?
  [e]
  (re-find operators (str e)))

(defn- numeric?
  [e]
  (or (number? e) (re-find numeric-pattern (str e))))

(defn- executable?
  "is this an executable element? (link or number)"
  [e]
  (or (numeric? e) (link? e)))

(defn- evaluate-operation
  [op a1 a2]
  (try
    (let [expr (str/join " " ["(" op a1 a2 ")"])
          res (eval (edn/read-string expr))]
      res)
    (catch ArithmeticException _ error)))

(defn- dereference-expression
  "dereference expression - either it's a value or link.
  if it's a link - evaluate that reference provided
  it's not auto-reference - in which case it's an error"
  [e cells acc derefs]
  (if (link? e)
    (if (contains? cells e)
      (if (contains? derefs e)
        error
        (or (get acc e) (parse-expression cells acc (get cells e) (conj derefs e))))
      error)
    e))

(defn- execute!
  "executes operation on stack,
  evaluates any links (cell addresses),
  returns error if any of the components is error"
  [op stack cells acc derefs]
  (if (> (count stack) 1)
    (let [a2 (dereference-expression (peek stack) cells acc derefs)
          a1 (dereference-expression (peek (pop stack)) cells acc derefs)]
      (if (some error? [a2 a1])
        error
        (evaluate-operation op a1 a2)))
    error))

(defn- complete?
  "is the iteration complete and if so what is the result"
  [tokens stack]
  (if (empty? tokens)
    (condp = (count stack)
      0 0
      1 (if-let [result (first stack)]
          (if (or (numeric? result) (link? result))
            result
            error))
      error)))

(defn parse-expression
  "Parse postfix expression resolving spreadsheet like addresses"
  [value-map acc expression derefs]
  (loop [tokens (apply list expression)
         stack []]
    (if-let [result (complete? tokens stack)]
      (dereference-expression result value-map acc derefs)
      (let [e (peek tokens)]
        (cond
          (executable? e) (recur (pop tokens) (conj stack e))
          (operator? e) (let [res (execute! e stack value-map acc derefs)]
                          (if (error? res)
                            error
                            (recur (pop tokens) (conj (pop (pop stack)) res))))
          :else error)))))

(defn assoc-parsed-expression
  [value-map acc [cell-address expression]]
  (let [res (parse-expression value-map acc expression #{cell-address})]
    (assoc acc cell-address res)))

(defn parse-expressions
  [value-map]
  (reduce (partial assoc-parsed-expression value-map) {} value-map))
