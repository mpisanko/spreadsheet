(ns spreadsheet.core-test
  (:require [clojure.test :refer :all]
            [spreadsheet.core :refer :all]
            [clojure.test.check.generators :as gen]
            [clojure.test.check.properties :as prop]
            [clojure.test.check.clojure-test :refer [defspec]]))

;(use-fixtures :once schema.test/validate-schemas)

;(defspec spanish 100
;  (prop/for-all [n gen/int]
;                (is (or (= :hola (say-hello)) (= :que-tal (say-hello))))))

