(ns spreadsheet.csv.converter-test
  (:require [clojure.test :refer :all]
            [spreadsheet.csv.converter :refer :all]
            [clojure.test.check.generators :as gen]
            [clojure.test.check.properties :as prop]
            [clojure.test.check.clojure-test :refer [defspec]]
            [clojure.string :as str]))

(def powers-of-26-gen
  (gen/fmap #(int (Math/pow 26 %)) (gen/choose 1 6)))

(def powers-of-26-less-1-gen
  (gen/fmap dec (gen/fmap #(int (Math/pow 26 %)) (gen/choose 1 6))))

(defspec column-parsing-powers 1e4
  (prop/for-all [power-of-26 powers-of-26-gen
                 powers-of-26-less-1 powers-of-26-less-1-gen]
    (is (re-find #"^ba{1,}$" (number-to-string power-of-26)))
    (is (re-find #"^z{1,}$" (number-to-string powers-of-26-less-1)))))

(defspec column-parsing-any 1e4
  (prop/for-all [col-num gen/s-pos-int]
    (is (re-find #"^[a-z]+$" (number-to-string col-num)))))

(defspec round-trip-parsing 1e4
  (prop/for-all [col-num gen/s-pos-int]
    (is (= (number-to-string col-num) (number-to-string (string-to-number (number-to-string col-num)))))))