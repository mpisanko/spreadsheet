(ns spreadsheet.csv.parse-test
  (:require [clojure.test :refer :all]
            [spreadsheet.csv.parse :refer :all]
            [clojure.test.check.generators :as gen]
            [clojure.test.check.properties :as prop]
            [clojure.test.check.clojure-test :refer [defspec]]
            [clojure.string :as str]))

(def csv-line-gen
  (gen/fmap
    (partial str/join "")
    (gen/vector
      (gen/one-of
        [(gen/elements [\, \. \+ \- \* \/]) gen/char-alpha])
      5 55)))

(defspec line-parsing 1e3
  (prop/for-all [line csv-line-gen]
    (is (=
          (count (parse-line line))
          (inc (count (re-seq value-separator line)))))))
