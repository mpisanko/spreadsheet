(ns spreadsheet.expression.parse-test
  (:require [clojure.test :refer :all]
            [spreadsheet.expression.parse :refer :all]
            [clojure.test.check.generators :as gen]
            [clojure.test.check.properties :as prop]
            [clojure.test.check.clojure-test :refer [defspec]]
            [clojure.string :as str]))


(def cell-address-gen
  (gen/fmap (comp str/lower-case (partial apply str)) (gen/tuple gen/char-alpha gen/nat)))

(def operator-gen
  (gen/one-of [(gen/elements ["+" "-" "*" "/"])]))

(def no-div-operator-gen
  (gen/one-of [(gen/elements ["+" "-" "*"])]))

(def finite-gen
  (gen/double* {:infinite? false :NaN? false}))

(def no-error-expression-gen
  (list gen/large-integer
        finite-gen
        no-div-operator-gen
        finite-gen
        gen/s-pos-int
        gen/s-pos-int
        gen/s-pos-int
        no-div-operator-gen
        no-div-operator-gen
        no-div-operator-gen
        no-div-operator-gen))

(def random-expression-gen
  (list gen/large-integer
        finite-gen
        no-div-operator-gen
        cell-address-gen
        operator-gen))

(defn expression-gen
  [& generators]
  (gen/fmap
       (partial mapv str)
       (apply gen/tuple generators)))

(defn vmap-gen
  [expression-generator]
  (gen/not-empty (gen/map cell-address-gen expression-generator)))

(def any-expression-gen
  (gen/not-empty (gen/vector (gen/one-of [gen/large-integer gen/int gen/double gen/string-alphanumeric]))))

(defspec no-error-expressions 1e2
         (prop/for-all [input (vmap-gen (apply expression-gen no-error-expression-gen))]
                       (let [results (map number? (vals (parse-expressions input)))]
                         (is (every? true? results)))))

(defspec output-has-all-input-keys 1e3
         (prop/for-all [input (vmap-gen (apply expression-gen random-expression-gen))]
                       (is (= (set (keys input)) (set (keys (parse-expressions input)))))))

(defspec empty-expression-translates-to-zero 1e2
         (prop/for-all [input (vmap-gen (gen/return []))]
                       (is (every? zero? (vals (parse-expressions input))))))

(defspec any-expression-evaluates-to-number-or-error 1e2
         (prop/for-all [input (vmap-gen any-expression-gen)]
                       (is (every? #(or (number? %) (re-find numeric-pattern (str %)) (= error %))
                                   (vals (parse-expressions input))))))

(def input-with-cycles
  {"a1" ["a2"] "a2" ["a3"] "a3" ["b1"] "a4" ["a1" "a2" "a3" "+"] "a5" ["b5"]
   "b1" ["a4"] "b2" ["3" "4" "*"] "b3" ["b2" "23" "-"] "b4" ["b3"] "b5" []})

(deftest resolves-cycles
  (let [result (parse-expressions input-with-cycles)
        errors (select-keys result ["a1" "a2" "a3" "a4" "b1"])
        successes (select-keys result ["a5" "b2" "b3" "b4" "b5"])]
    (is (every? (partial = error) (map (partial re-find error-pattern) (vals errors))))
    (is (every? number? (vals successes)))))